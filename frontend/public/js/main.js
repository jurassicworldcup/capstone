$(function () {
  var keywordsContainer = $('#trending-keywords');
  var loading = keywordsContainer.find('.loading');

  function updateKeywords(list) {
    keywordsContainer.empty();

    keywordsContainer.append(list.map(function (keyword, index) {
      var rank = index + 1;
      return $("<div class='keyword-item'>")
        .append($("<div class='rank'>").text(rank))
        .append($("<div class='keyword'>").text(decodeURI(keyword)));
    }));
  }

  var ws = new WebSocket('ws://127.0.0.1:9003');
  ws.onopen = function () {
    loading.text('connected...');
  };
  ws.onmessage = function (e) {
    updateKeywords(JSON.parse(e.data));
  };
});
