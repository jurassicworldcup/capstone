$(function () {
  var keywordsContainer = $('#trending-keywords');
  var loading = keywordsContainer.find('.loading');

  function updateKeywords(list) {
    var keyword = list[0];

    if (keyword.length == 0) {
      return;
    }

    loading.remove();

    keywordsContainer.prepend($("<div class='keyword-item'>")
        .append($("<div class='keyword'>").text(decodeURI(keyword))));

    var items = keywordsContainer.find('.keyword-item');
    if (items.length > 10) {
      for (var i = 10; i < items.length; i++) {
        items[i].remove();
      }
    }
  }

  function updateOmitted(count) {
    $('#omitted').text(count);
  }

  var ws = new WebSocket('ws://127.0.0.1:9003');
  ws.onopen = function () {
    loading.text('connected...');
  };
  ws.onmessage = function (e) {
    var data = JSON.parse(e.data);
    updateKeywords(data.keywords);
    updateOmitted(data.omitted);
  };
});
