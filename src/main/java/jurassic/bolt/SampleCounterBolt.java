package jurassic.bolt;

import backtype.storm.Config;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

// FIXME: Cannot find backtype.storm.utils.TupleUtils
import jurassic.utils.TupleUtils;

import java.util.HashMap;
import java.util.Map;

public class SampleCounterBolt extends BaseBasicBolt {

  private final int emitFrequencyInSeconds;
  private final String targetWord;

  private int wordOccurence;
  private int previousWordOccurence;

  public SampleCounterBolt(String targetWord, int emitFrequencyInSeconds) {
    this.targetWord = targetWord;
    this.emitFrequencyInSeconds = emitFrequencyInSeconds;
  }

  @Override
  public void execute(Tuple tuple, BasicOutputCollector collector) {
    if (TupleUtils.isTick(tuple)) {
      collector.emit(new Values(wordOccurence, (wordOccurence - previousWordOccurence) * emitFrequencyInSeconds));
      previousWordOccurence = wordOccurence;
      return;
    }

    if (tuple.getStringByField("data").contains(targetWord)) {
      wordOccurence += 1;
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields("count", "occurence_rate"));
  }

  @Override
  public Map<String, Object> getComponentConfiguration() {
    Map<String, Object> conf = new HashMap<String, Object>();
    conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, emitFrequencyInSeconds);
    return conf;
  }
}
