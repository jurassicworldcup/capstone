package jurassic.bolt;

import backtype.storm.tuple.Tuple;
import jurassic.Trend;

import java.util.*;

public class Aggregator extends Node {
    private long latestTimestamp;
    private final long WINDOW_SIZE_IN_MILLISECONDS = 10 * 1000;
    private List<Tuple> buffer;
    private final boolean isVerbose;

    public Aggregator() {
        this(true);
    }

    public Aggregator(boolean isVerbose) {
        super();
        this.isVerbose = isVerbose;
        buffer = new LinkedList<Tuple>();
    }

    private void log(String s) {
        if (isVerbose) {
            System.out.print(s);
        }
    }

    @Override
    protected Trend getTrend() {
        HashMap<String, Integer> trend = new HashMap<String, Integer>();
        for (Tuple tuple: buffer) {
            final String keyword = tuple.getStringByField("keyword");
            final Integer trendiness = tuple.getIntegerByField("trendiness");
            final Integer value = trend.get(keyword);
            if (value == null) {
                trend.put(keyword, trendiness);
            } else {
                trend.put(keyword, trendiness + value);
            }
        }

        // FIXME: tie score?
        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry : trend.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        if (maxEntry == null) {
            return new Trend.EmptyTrend(latestTimestamp);
        }
        log(" " + maxEntry.getKey() + " \n");
        buffer = new LinkedList<Tuple>();
        return new Trend(maxEntry.getKey(), maxEntry.getValue(), latestTimestamp);
    }

    private void flushOldTuple() {
        Iterator<Tuple> it = buffer.iterator();
        while (it.hasNext()) {
            if (isTupleOld(it.next())) {
                it.remove();
            }
        }
    }

    private boolean isTupleOld(Tuple tuple) {
        final Long timestamp = tuple.getLongByField("timestamp");
        return latestTimestamp - timestamp > WINDOW_SIZE_IN_MILLISECONDS;
    }

    @Override
    protected void initialize() {
    }

    @Override
    protected void clean() {
    }

    @Override
    protected OnTupleReturnType onTuple(Tuple tuple){
        // FIXME: can reduce the number of tuple.getLongByField("timestamp") called if needed
        if (isTupleOld(tuple)) {
            log("_");
            return OnTupleReturnType.OMIT;
        }

        if (!Aggregator.isTupleEmpty(tuple)) {
            log("*");
            final Long timestamp = tuple.getLongByField("timestamp");
            if (timestamp > latestTimestamp) {
                latestTimestamp = timestamp;
                flushOldTuple();
            }

            buffer.add(tuple);
        }
        return OnTupleReturnType.NO_OMIT;
    }
}

