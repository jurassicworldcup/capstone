package jurassic.bolt;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

import java.io.PrintWriter;
import java.util.Map;


public class LogBolt extends BaseBasicBolt {
    private String fileName;
    private PrintWriter writer;

    public LogBolt(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        super.prepare(stormConf, context);
        try {
            writer = new PrintWriter("log/" + fileName + ".log", "UTF-8");
        } catch (Exception ex) {
            writer = null;
        }
    }

    @Override
    public void cleanup() {
        if (writer != null) {
            writer.close();
        }
        super.cleanup();
    }

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        // FIXME: writing a file is not working...
        writer.println(tuple);
        collector.emit(tuple.getValues());
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("data"));
    }
}
