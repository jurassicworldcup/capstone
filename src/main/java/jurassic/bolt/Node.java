package jurassic.bolt;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import jurassic.Trend;
import java.util.Map;

// FIXME: replace BaseBasicBolt to IBolt
// http://user.storm.apache.narkive.com/B3A2DouB/bolt-type-question
public abstract class Node extends BaseBasicBolt {
    protected abstract void initialize();
    protected abstract void clean();
    protected abstract Trend getTrend();
    protected abstract OnTupleReturnType onTuple(Tuple tuple);

    public enum OnTupleReturnType {
        OMIT,
        NO_OMIT
    }

    public static boolean isTupleEmpty(Tuple tuple) {
        return tuple.getIntegerByField("trendiness") == 0;
    }


    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        super.prepare(stormConf, context);
        initialize();
    }

    @Override
    public void cleanup() {
        clean();
        super.cleanup();
    }

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        int count = tuple.getIntegerByField("omitted");
        if (onTuple(tuple) == OnTupleReturnType.OMIT) {
            count += 1;
        }

        Trend t = getTrend();
        collector.emit(new Values(t.keyword, t.trendiness, t.timestamp, count));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("keyword", "trendiness", "timestamp", "omitted"));
    }
}
