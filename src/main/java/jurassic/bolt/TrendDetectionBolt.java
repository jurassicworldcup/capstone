package jurassic.bolt;

import backtype.storm.tuple.Tuple;

import jurassic.Trend;
import jurassic.algorithm.TrendDetector;
import org.javatuples.Triplet;

public class TrendDetectionBolt extends Node {
    private TrendDetector detector;

    private long latestTimestamp = 0;
    private long tupleTimestamp = 0;

    @Override
    protected void initialize() {
        detector = new TrendDetector();
    }

    @Override
    protected void clean() {
    }

    @Override
    protected OnTupleReturnType onTuple(Tuple tuple) {
        tupleTimestamp = tuple.getLongByField("timestamp");
        if (latestTimestamp < tupleTimestamp){
            latestTimestamp = tupleTimestamp;
        }
        boolean omitted = detector.handleEvent(tuple.getStringByField("keyword"), tupleTimestamp);
        return omitted ? OnTupleReturnType.OMIT : OnTupleReturnType.NO_OMIT;
    }

    @Override
    protected Trend getTrend() {
        Triplet<String, Integer, Long> keyword = null;

        try {
            keyword = detector.getTrend(latestTimestamp);
        } catch (IndexOutOfBoundsException e) {
            // do nothing
        }

        if (keyword == null) {
            return new Trend.EmptyTrend(tupleTimestamp);
        }

        return new Trend(keyword.getValue0(), keyword.getValue1(), tupleTimestamp);
    }
}
