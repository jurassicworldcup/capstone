package jurassic.bolt;

import java.net.UnknownHostException;
import java.util.Map;

import org.java_websocket.WebSocketImpl;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

import jurassic.lib.TrendWebSocketServer;

public class WebSocketBolt extends BaseBasicBolt {
    static final int PORT_NUMBER = 9003;

    private TrendWebSocketServer server;
    private int omitted = 0;

    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        super.prepare(stormConf, context);
        WebSocketImpl.DEBUG = false;
        try {
            server = new TrendWebSocketServer(PORT_NUMBER);
            server.start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        if (server == null) {
            return;
        }

        omitted += tuple.getIntegerByField("omitted");
        server.broadcast("{\"omitted\": " + omitted + ", \"keywords\": [\"" + tuple.getStringByField("keyword") + "\"]}");
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer ofd) {
    }
}
