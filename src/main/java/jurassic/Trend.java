package jurassic;

public class Trend {
    public final String keyword;
    public final Integer trendiness;
    public final Long timestamp;

    public Trend(String keyword, Integer trendiness, Long timestamp) {
        this.keyword = keyword;
        this.trendiness = trendiness;
        this.timestamp = timestamp;
    }

    public static class EmptyTrend extends Trend {
        public EmptyTrend(Long timestamp) {
            super("", 0, timestamp);
        }
    }
}
