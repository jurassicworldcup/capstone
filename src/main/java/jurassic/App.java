package jurassic;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import jurassic.spout.RabbitMQSpout;
import jurassic.spout.StandaloneSpout;

public class App
{
    public static void main( String[] args )
    {
        StandaloneSpout spout = new StandaloneSpout();
        Config conf = new Config();
        conf.put(Config.TOPOLOGY_DEBUG, false);
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("sample", conf, TopologyFactory.BinaryTopology(spout, 3));
    }
}
