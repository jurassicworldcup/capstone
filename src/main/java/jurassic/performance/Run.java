package jurassic.performance;

import jurassic.bolt.Aggregator;
import jurassic.bolt.TrendDetectionBolt;
import jurassic.spout.MockComputationNode;
import jurassic.spout.StandaloneSpout;

public class Run {
    public static void main(String[] args) {
        PerformanceTestFactory.Throughput(new MockComputationNode(), 1);
        PerformanceTestFactory.ThroughputWithMockComputationSpout(new TrendDetectionBolt(), 1);
        PerformanceTestFactory.ThroughputWithMockTrendDetectionSpout(new Aggregator(false), 100);
        PerformanceTestFactory.ThroughputOfBinaryTopology(new MockComputationNode(), 2, 100);

    }
}
