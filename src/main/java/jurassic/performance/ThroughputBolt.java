package jurassic.performance;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import jurassic.utils.TupleUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yong on 15. 11. 17..
 */

public class ThroughputBolt extends BaseBasicBolt {
    private int count;

    public ThroughputBolt() { }

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
        if (TupleUtils.isTick(tuple)) {
            collector.emit(new Values(count));
            count = 0;
            return;
        }

        count += 1;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("throughput"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        final int EMIT_FREQUENCY_IN_SECS = 1;
        Map<String, Object> conf = new HashMap<String, Object>();
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, EMIT_FREQUENCY_IN_SECS);
        return conf;
    }
}
