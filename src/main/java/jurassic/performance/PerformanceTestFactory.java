package jurassic.performance;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.utils.Utils;
import jurassic.TopologyFactory;
import jurassic.bolt.*;
import jurassic.spout.MockComputationNode;
import jurassic.spout.MockTrendDetectionSpout;

import java.util.Arrays;

public class PerformanceTestFactory {
    public static void Throughput(IRichSpout spout, int testSeconds) {
        Config conf = new Config();
        conf.put(Config.TOPOLOGY_DEBUG, false);
        LocalCluster cluster = new LocalCluster();

        TopologyBuilder builder = new TopologyBuilder();
        String sourceId = "src";
        builder.setSpout(sourceId, spout);
        builder.setBolt("test", new ThroughputBolt()).shuffleGrouping(sourceId);
        builder.setBolt("print", new PrinterBolt()).shuffleGrouping("test");

        cluster.submitTopology("sample", conf, builder.createTopology());
        Utils.sleep(testSeconds * 1000);
        cluster.killTopology("sample");
        cluster.shutdown();
    }

    private static void Throughput(BaseBasicBolt bolt, IRichSpout testSpout, int testSeconds) {
        Config conf = new Config();
        conf.put(Config.TOPOLOGY_DEBUG, false);
        LocalCluster cluster = new LocalCluster();

        TopologyBuilder builder = new TopologyBuilder();
        String sourceId = "src";
        builder.setSpout(sourceId, testSpout);
        builder.setBolt("bolt", bolt).shuffleGrouping(sourceId);
        builder.setBolt("test", new ThroughputBolt()).shuffleGrouping("bolt");
        builder.setBolt("print", new PrinterBolt()).shuffleGrouping("test");

        cluster.submitTopology("sample", conf, builder.createTopology());
        Utils.sleep(testSeconds * 1000);
        cluster.killTopology("sample");
        cluster.shutdown();
    }

    public static void ThroughputWithMockComputationSpout(BaseBasicBolt bolt, int testSeconds) {
        Throughput(bolt, new MockComputationNode(), testSeconds);
    }

    public static void ThroughputWithMockTrendDetectionSpout(BaseBasicBolt bolt, int testSeconds) {
        Throughput(bolt, new MockTrendDetectionSpout(), testSeconds);
    }

    public static void ThroughputOfBinaryTopology(IRichSpout input, int n, int testSeconds) {
        Config conf = new Config();
        conf.put(Config.TOPOLOGY_DEBUG, false);
        LocalCluster cluster = new LocalCluster();

        StormTopology topology = TopologyFactory.BinaryTopology(input, n, Arrays.asList(new ThroughputBolt(), new PrinterBolt()));

        cluster.submitTopology("sample", conf, topology);
        Utils.sleep(testSeconds * 1000);
        cluster.killTopology("sample");
        cluster.shutdown();
    }

}