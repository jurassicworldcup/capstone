package jurassic.lib;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class TrendWebSocketServer extends WebSocketServer {
    Set<WebSocket> clients = new HashSet<WebSocket>();

    public TrendWebSocketServer(int port) throws UnknownHostException {
        super(new InetSocketAddress(port), Collections.<Draft>singletonList(new Draft_17()));
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        clients.add(conn);
    }

    @Override
    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
        clients.remove(conn);
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        // do nothing
    }

    @Override
    public void onError( WebSocket conn, Exception ex ) {
        System.out.println( "Error:" );
        ex.printStackTrace();
    }

    public void broadcast(String message) {
        for (WebSocket client : clients) {
            client.send(message);
        }
    }
}
