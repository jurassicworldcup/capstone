package jurassic.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.util.Map;
import java.util.Random;

public class MockTrendDetectionSpout extends BaseRichSpout {
    SpoutOutputCollector _collector;
    Random _rand;
    int _count;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        _collector = collector;
        _rand = new Random();
    }

    @Override
    public void nextTuple() {
        final int logInterval = 10000;
        String[] keywords = new String[]{
                "github",
                "stackexchange",
                "uber",
                "dropbox",
                "google",
                "guro",
                "princeton",
                "jck",
                "spoqa",
                "sansung",
                "xiaomi"
        };
        String keyword = keywords[_rand.nextInt(keywords.length)];
        _count += 1;
        if (_count == logInterval) {
            System.out.println("emitted " + _count + " tuples, current is " + keyword);
            _count = 0;
        }

        final int trendinessBound = 100;
        _collector.emit(new Values(keyword, _rand.nextInt(trendinessBound), System.currentTimeMillis(), 0));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("keyword", "trendiness", "timestamp", "omitted"));
    }
}
