package jurassic.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class StandaloneSpout extends BaseRichSpout {
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private final static String ENCODING = "UTF-8";

    private SpoutOutputCollector mCollector;

    ArrayList<String> mKeywords =  new ArrayList<>();
    private int mCurrentTupleIndex = 0;
    private int mLastTupleIndex = 0;

    // Should be assigned by UI.
    private String mIntendedTrendKeyword = "google";

    // In milliseconds.
    private final static long INTENDED_KEYWORD_EMISSION_INTERVAL = 1;
    private long mLastEmissionOfIntendedKeyword = 0;

    private final static long INFORMATION_PRINTING_INTERVAL = 1000 * 10;
    private long mLastInformationPrinting = 0;

    private long mEmissionStartTime = 0;
    private int mIntendedKeywordEmissionLoopCount = 0;
    private int mCurrentIntendedKeywordEmittedLoop = 0;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.mCollector = collector;

        try {
            initializePageList();
            // System.out.println("Intended keyword should become trend after 20 seconds.");
            // Timer activeIntendedKeywordScheduler = new Timer();
            // activeIntendedKeywordScheduler.schedule(new ActivateIntendedKeywordTask("apple", 20), 1000 * 20);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void nextTuple() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - mLastInformationPrinting >= INFORMATION_PRINTING_INTERVAL) {

            if (mEmissionStartTime == 0)
                mEmissionStartTime = currentTime;

            long totalSeconds = System.currentTimeMillis() - mEmissionStartTime;
            System.out.println("Emitted tuples: " + mCurrentTupleIndex
                    + " in " + totalSeconds / 1000 + " seconds. In latest 10 secs : "
                    + (mCurrentTupleIndex - mLastTupleIndex) / ((currentTime - mLastInformationPrinting) / 1000.0) + " tuples/sec");
            mLastInformationPrinting = System.currentTimeMillis();
            mLastTupleIndex = mCurrentTupleIndex;
        }

        // TODO: Currently, do not check bound of list, because of experiment.(Avoid branch)
        mCollector.emit(new Values(mKeywords.get(mCurrentTupleIndex++), System.currentTimeMillis(), 0));

        /* Turn on if activeIntendedKeywordScheduler.schedule used in open().
        if (mIntendedKeywordEmissionLoopCount == 0 ||
                mLastEmissionOfIntendedKeyword + INTENDED_KEYWORD_EMISSION_INTERVAL >= System.currentTimeMillis()) {
            mCollector.emit(new Values(keyword, System.currentTimeMillis(), 0));
            mCurrentTupleIndex++;
            return;
        }

        if (mIntendedKeywordEmissionLoopCount != 0 &&
                mCurrentIntendedKeywordEmittedLoop < mIntendedKeywordEmissionLoopCount) {
            mCollector.emit(new Values(mIntendedTrendKeyword, System.currentTimeMillis(), 0));
            mCurrentIntendedKeywordEmittedLoop++;
            return;
        }

        mLastEmissionOfIntendedKeyword = System.currentTimeMillis();
        mCurrentIntendedKeywordEmittedLoop = 0;
        */
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("keyword", "timestamp", "omitted"));
    }

    private void initializePageList() throws IOException {
        // Loads lookup file.
        BufferedReader reader = null;

        try {
            File dataFolder = new File("WikiPageData");
            // Ensure to read proper data files.
            File[] refinedDataFiles = dataFolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith("pagecounts-2011");
                }
            });
            Arrays.sort(refinedDataFiles);

            System.out.println("Start loading... " + DATE_FORMAT.format(Calendar.getInstance().getTime()));
            for (final File fileEntry : refinedDataFiles) {
                if (fileEntry.isDirectory())
                    continue;

                String lookupFileName = dataFolder.getName() + "/lookup/" + fileEntry.getName() + "-lookup";
                File lookupFile = new File(lookupFileName);
                System.out.println("lookup file: " + lookupFileName + " " + DATE_FORMAT.format(Calendar.getInstance().getTime()));

                HashMap<Integer, String> indexPageMap = new HashMap<>();
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(lookupFile), ENCODING));
                int pageIndex = 0;
                for (String line; (line = reader.readLine()) != null;) {
                    indexPageMap.put(pageIndex++, line);
                }

                System.out.println("data file: " + fileEntry.getName() + " " + DATE_FORMAT.format(Calendar.getInstance().getTime()));
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileEntry), ENCODING));
                for (String line; (line = reader.readLine()) != null;) {
                    mKeywords.add(indexPageMap.get(Integer.parseInt(line)));
                }

                System.out.println("End loading. " + DATE_FORMAT.format(Calendar.getInstance().getTime()));
            }
        } finally {
            reader.close();
        }
    }

    private void activateIntendedKeyword (String intended, int loopCount) {
        mIntendedTrendKeyword = intended;
        mIntendedKeywordEmissionLoopCount = loopCount;
    }

    // Dummy. Trigger activateIntendedKeyword.
    class ActivateIntendedKeywordTask extends TimerTask {
        String keyword;
        int loop;
        ActivateIntendedKeywordTask(String keyword, int loop) {
            this.keyword = keyword;
            this.loop = loop;
        }
        public void run() {
            activateIntendedKeyword(keyword, loop);
        }
    }
}
