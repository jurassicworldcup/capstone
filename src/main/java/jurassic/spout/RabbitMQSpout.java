package jurassic.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.GetResponse;
import org.javatuples.Pair;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class RabbitMQSpout extends BaseRichSpout {
    private SpoutOutputCollector collector;
    private Channel channel;

    private final static String QUEUE_NAME = "jurassic";

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        try {
            channel = connectToRabbitMQ();
        } catch (Exception e) {
            // FIXME: ok?
            System.out.println(e);
            System.exit(1);
        }
    }

    @Override
    public void nextTuple() {
        try {
            boolean autoAck = true;
            GetResponse response = channel.basicGet(QUEUE_NAME, autoAck);
            if (response == null) {
                System.out.print(",");
                return;
            }
            Pair<String, Long> body = parseResponse(response);
            collector.emit(new Values(body.getValue0(), body.getValue1(), 0));
        } catch (Exception e) {
            // FIXME: ok?
            System.out.println(e);
        }
    }

    private Pair<String, Long> parseResponse(GetResponse res) throws UnsupportedEncodingException {
        String body = new String(res.getBody(), "UTF-8");
        int separatorIdx = body.lastIndexOf('|');
        String keyword = body.substring(0, separatorIdx);
        Long timestamp = Long.parseLong(body.substring(separatorIdx + 1));
        return new Pair<String, Long>(keyword, System.currentTimeMillis());
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("keyword", "timestamp", "omitted"));
    }

    private Channel connectToRabbitMQ() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        return channel;
    }
}

