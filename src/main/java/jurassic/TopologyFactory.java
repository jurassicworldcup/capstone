package jurassic;

import backtype.storm.generated.StormTopology;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import jurassic.bolt.Aggregator;
import jurassic.bolt.TrendDetectionBolt;
import jurassic.bolt.WebSocketBolt;

import java.util.Arrays;
import java.util.List;

public class TopologyFactory {

    public static StormTopology BinaryTopology(IRichSpout input, int n) {
        return BinaryTopology(input, n, Arrays.asList((BaseBasicBolt)new WebSocketBolt()));
    }

    public static StormTopology BinaryTopology(IRichSpout input, int n, List<BaseBasicBolt> boltList) {
        TopologyBuilder builder = new TopologyBuilder();
        String sourceId = "src";
        builder.setSpout(sourceId, input);

        String trendDetectionId = "trend-detection";
        builder.setBolt(trendDetectionId, new TrendDetectionBolt(), Math.pow(2, n)).shuffleGrouping(sourceId);

        String prev = trendDetectionId;
        for (int i = n - 1; i >= 0; i--) {
            String current = "layer" + i;
            builder.setBolt(current, new Aggregator(false), Math.pow(2, i)).shuffleGrouping(prev);
            prev = current;
            System.out.println("Create aggregator layer [" + current+ "] (size: " + Math.pow(2, i) + ")");
        }

        for (int i = 0; i < boltList.size(); i++) {
            String current = "end" + i;
            builder.setBolt(current, boltList.get(i)).shuffleGrouping(prev);
            prev = current;
            System.out.println("Connect bolt [" + current+ "]");
        }

        return builder.createTopology();
    }

}
