package jurassic.algorithm;

import org.javatuples.Triplet;

import java.util.*;

public class TrendDetector {
    public static final int BUCKET_INTERVAL = 10; // sec
    public static final int EVENT_COUNT_THRESHOLD = 20;
    public static final double CONFIDENCE_INTERVAL_CONSTANT = 2.58; // for 0.99 confidence level
    public static final double UNLIKELINESS = 1.0;
    public static final int MAXIMUM_KEYWORD_COUNT = 10;

    private Map<String, TimeBucket> keywordBuckets = new HashMap<String, TimeBucket>();
    private List<Triplet<String, Integer, Long>> trendingList = new LinkedList<Triplet<String, Integer, Long>>();

    public TrendDetector() {
    }

    private TimeBucket getBucket(String keyword) {
        TimeBucket b = keywordBuckets.get(keyword);
        if (b == null) {
            b = new TimeBucket(BUCKET_INTERVAL);
            keywordBuckets.put(keyword, b);
        }
        return b;
    }

    public boolean handleEvent(String keyword, long timestamp) {
        TimeBucket b = getBucket(keyword);

        boolean omitted = b.count(timestamp);

        long previousCount = b.getPreviousCount();
        long currentCount = b.getCurrentCount();

        if (previousCount == 0 || currentCount < EVENT_COUNT_THRESHOLD) {
            return omitted;
        }

        double trendLevel = calculateTrend(previousCount, currentCount);

        if (trendLevel > UNLIKELINESS * CONFIDENCE_INTERVAL_CONSTANT) {
            considerKeyword(new Triplet<String, Integer, Long>(keyword, (int)(trendLevel * 1000), timestamp));
        }

        return omitted;
    }

    private double calculateTrend(double mean, double observation) {
        return (observation - mean) / Math.sqrt(mean / observation);
    }

    public void considerKeyword(Triplet<String, Integer, Long> keywordTriplet) {
        Iterator it = trendingList.iterator();
        while (it.hasNext()) {
            Triplet<String, Integer, Long> trend = (Triplet<String, Integer, Long>)it.next();

            if (trend.getValue0() == keywordTriplet.getValue0()) {
                it.remove();
            }
        }
        trendingList.add(keywordTriplet);

        // sort the trending list in descending order
        Collections.sort(trendingList, new Comparator<Triplet<String, Integer, Long>>() {
            @Override
            public int compare(Triplet<String, Integer, Long> o1, Triplet<String, Integer, Long> o2) {
                return -1 * o1.getValue1().compareTo(o2.getValue1());
            }
        });

        if (trendingList.size() > MAXIMUM_KEYWORD_COUNT) {
            trendingList = new LinkedList<Triplet<String, Integer, Long>>(trendingList.subList(0, MAXIMUM_KEYWORD_COUNT));
        }
    }

    public Triplet<String, Integer, Long> getTrend(long latestTimestamp) {
        Iterator it = trendingList.iterator();
        while (it.hasNext()) {
            Triplet<String, Integer, Long> trend = (Triplet<String, Integer, Long>)it.next();
            if (trend.getValue2() < latestTimestamp - BUCKET_INTERVAL) {
                it.remove();
            }
        }

        return trendingList.get(0);
    }
}
