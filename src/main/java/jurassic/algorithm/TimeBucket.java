package jurassic.algorithm;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TimeBucket {
    private int interval;

    private List<Long> occurrence = new LinkedList<Long>();
    private long previousCount = 0;
    private long currentCount = 0;

    private long currentTimestamp = 0;

    public TimeBucket(int interval) {
        this.interval = interval;
    }

    public boolean count(long timestamp) {
        previousCount = 0;
        currentCount = 0;

        boolean omitted = false;

        if (timestamp < currentTimestamp - interval * 2000) {
            omitted = true;
        } else {
            if (timestamp > currentTimestamp) {
                currentTimestamp = timestamp;
            }

            // prepend
            occurrence.add(timestamp);
        }

        Iterator it = occurrence.iterator();
        while (it.hasNext()) {
            long itTimestamp = (Long)it.next();
            if (itTimestamp < currentTimestamp - interval * 2000) {
                it.remove();
            } else if (itTimestamp < currentTimestamp - interval * 1000) {
                previousCount++;
            } else {
                currentCount++;
            }
        }

        return omitted;
    }

    public long getPreviousCount() {
        return previousCount;
    }

    public long getCurrentCount() {
        return currentCount;
    }
}
